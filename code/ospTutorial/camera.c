OSPCamera camera = ospNewCamera("perspective");

ospSet1f(camera, "aspect", imgSize.x/imgSize.y);
ospSet3fv(camera, "pos", cam_pos);
ospSet3fv(camera, "dir", cam_view);
ospSet3fv(camera, "up",  cam_up);

ospCommit(camera);

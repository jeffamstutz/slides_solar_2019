OSPFrameBuffer framebuffer = ospNewFrameBuffer(
  vec2i(1024, 768),
  OSP_FB_SRGBA,
  OSP_FB_COLOR | OSP_FB_ACCUM
);

ospResetAccumulation(framebuffer);

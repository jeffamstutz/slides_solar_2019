OSPGeometryInstance instance = ospNewGeometryInstance(spheres);

OSPMaterial mat = ospNewMaterial("scivis", "default");
ospSet3f(mat, "color", 1.f, 0.f, 0.f);

ospCommit(mat);

ospSetMaterial(instance, mat);

ospCommit(instance);

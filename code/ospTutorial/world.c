OSPWorld world = ospNewWorld();

OSPData geometryInstances =
  ospNewData(1, OSP_OBJECT, &instance, 0);

ospSetObject(world, "geometries", geometryInstances);

ospCommit(world);

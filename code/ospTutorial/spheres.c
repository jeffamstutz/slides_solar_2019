vec4f vertex[] = {{0.f, 0.f,  3.f, 0.f}
                  {0.f, 0.f, -3.f, 0.f}};

OSPGeometry spheres = ospNewGeometry("spheres");

OSPData data = ospNewData(2, OSP_FLOAT4, vertex);
ospSetData(spheres, "spheres", data);

ospSet1f(spheres, "radius", 1.0f);

ospCommit(spheres);

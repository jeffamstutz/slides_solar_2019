# Ray tracing and rendering in 2019

Jefferson Amstutz, Intel

SOLAR 2019

---

### Outline

- CPU Ray Tracing For Simulation
- OSPRay API
  - Objects + Types
  - Parameters
  - Tutorial
- A Proposed Cross-Vendor Rendering Stack

---

# Ray Tracing for Simulation

---

### What are some simulations that use ray tracing?

@ul
- Ballistic penetration
- Radio frequency propagation
- Thermal radiance transfer
- Particle radiation
- ...and many others!
@ulend

---

### Ray tracing --> ray/surface queries

---

![logo](assets/img/ray_queries/Slide1.png)

---

![logo](assets/img/ray_queries/Slide2.png)

---

### But how do you make rays fast?

---

![logo](assets/img/bvh_2d.jpeg)

---

![logo](assets/img/bvh_3d.png)

---

# What libraries are available to trace rays?

@box[fragment](_Your HW + SW vendors are here to help!_)

---

### Hardware Vendor RT Packages

@ul
- Intel: @css[text-blue](Embree)
- NVIDIA: @css[text-green](OptiX)
- AMD: @css[text-orange](Radeon Rays)
@ulend

---

### Ray kernel libraries solve:

@ol
- Acceleration structure construction
- Acceleration structure ray traversal
- Ray/primitive intersection
@olend

---

### You get to focus on your physics, not the rays!

---

### What is the simulation still responsible for?

@ul
- Creating the surfaces the rays intersect
- The physics that the rays represent
- Feeding ray tracing libraries efficiently
- ...others?
@ulend

---

# Feeding ray tracing libraries?!?!

---

![logo](assets/img/ballistic_sim.png)

---

![logo](assets/img/ballistic_compute.jpg)

---

### Ray marching --> volumetric interactions

---

![logo](assets/img/ray_marching.png)

---

![logo](assets/img/volume_intersect_1.png)

---

![logo](assets/img/volume_intersect_2.png)

---

![logo](assets/img/volume_marching_1.png)

---

![logo](assets/img/volume_marching_2.png)

---

![logo](assets/img/volume_marching_3.png)

---

Rendering is just as much of a simulation as any other domain.

---

![logo](assets/img/ray_tracing_1.png)

---

![logo](assets/img/ray_tracing_2.png)

---

# The OSPRay API

---

### OSPRay is known to be:

@ul
- ...a ray tracing API
- ...a set of ray tracing infrastructure
- ...a scalable rendering engine
@ulend

---

# There is so much more!

---

### OSPRay API vs. OSPRay Impl.

@ul
- OSPRay defines a very generic @css[text-yellow](render graph) API.

- OSPRay also implements this API with @css[text-yellow](multiple rendering backends).
@ulend

---

### _OSPRay API_ vs. OSPRay Impl.

OSPRay provides an API that is agnostic to:

@ul
- ...rendering @css[text-yellow](algorithm) (raster vs. rays)
- ...underlying @css[text-yellow](hardware) (CPU vs. GPU)
- ...underlying @css[text-yellow](scale) (local vs. remote vs. cluster)
@ulend

---

### OSPRay API vs. _OSPRay Impl._

The OSPRay project implements the API to be:

@ul
- ...a threaded/vectorized CPU ray tracing engine
    - using ISPC and TBB
- ...tuned for the CPU you are running on
    - both now and in the future
- ...scalable across a cluster via MPI
    - both in compute and total memory
@ulend

---

# The OSPRay API

---

## Everything is about exactly one function call.

---

```c++
void ospRenderFrame(
  OSPFrameBuffer,
  OSPRenderer,
  OSPCamera,
  OSPWorld
);
```

@[1-6](The goal is to fill out the tuple of objects to render a frame.)
@[2](Frame buffer...)
@[3](Renderer...)
@[4](Camera...)
@[5](...and World!)

---

### General OSPRay object usage:

@ul
- All OSPRay objects are controlled by opaque handles
    - Logically "just a pointer"
- All OSPRay objects take "`named`" parameters
@ulend

---

### General OSPRay object usage:

- Generally follow a create/set/commit pattern:

@ol
- Instantiation (via _`ospNew*()`_ API calls)
- Set parameters (via _`ospSet*()`_ API calls)
- Commit (via _`ospCommit()`_)
@olend

@box[fragment](_NOTE:_ to update parameters, repeat steps 2 and 3)

---

### Object Types

- Data arrays (_`OSPData`_)
- Frame buffer (_`OSPFrameBuffer`_)
- Renderer (_`OSPRenderer`_)
- Camera (_`OSPCamera`_)
- Geometry (_`OSPGeometry`_)
- Volume (_`OSPVolume`_)
- Instances (_`OSPGeometryInstance`_, _`OSPVolumeInstance`_)

---

### Object Types (cont.)

- Material (_`OSPMaterial`_)
- Texture (_`OSPTexture`_)
- Transfer function (_`OSPTransferFunction`_)
- Light (_`OSPLight`_)
- World (_`OSPWorld`_)
- PixelOp (_`OSPPixelOp`_)
- Future (_`OSPFuture`_)

---

### Data Arrays (_`OSPData`_)

@ul
- Describe an array of data from the application
- May or may not own the memory passed on construction
- Can be primitive types (i.e. _`float`_), or OSPRay handles
@ulend

---

```c++
std::array<float, 2> myFloats = {0.f, 1.f};

OSPData data_handle = ospNewData(
  myFloats.size(), // number of elements (2)
  OSP_FLOAT,       // element type (float)
  myFloats.data,   // actual data
  0,               // no flags -> copy into private array
);
```

@[7](OSPRay copies the input data on construction into a private array)

---

# A Quick OSPRay Tutorial

---

### Steps to use OSPRay

@ol
- Initialize OSPRay
- Create a frame buffer
- Create a camera
- Create a renderer
- Create a world*
@olend

@box[fragment](*requires multiple steps)yy

---

### Initialize OSPRay

@code[c++](code/ospTutorial/init.c)

@[1](First, initialize OSPRay)
@[3-5](..and check to see if there was an error)

---

### Create the frame buffer

@code[c++](code/ospTutorial/framebuffer.c)

@[1-5](All frame buffer parameters are passed on construction.)
@[7](Frames are "cleared" with an API call.)

---

### Create the camera

@code[c++](code/ospTutorial/camera.c)

@[1](Create an instance of a camera.)
@[3-6](Set the camera's desired parameters.)
@[8](And finally, commit it.)

---

### Create the renderer

@code[c++](code/ospTutorial/renderer.c)

@[1](Create an instance of a renderer.)
@[3-4](Set the renderer parameters.)
@[6](And commit it.)

---

### Now create the scene

@box[fragment](_NOTE:_ it takes more than one slide...)

---

### Create spheres geometry

@code[c++](code/ospTutorial/spheres.c)

@[1-2](...given some input geometry data.)
@[4](Create the geometry object.)
@[6-7](Create a data array of the sphere centers.)
@[9](Set the global radius as a parameter.)
@[11](And finally commit the geometry.)

---

### Create geometry instance

@code[c++](code/ospTutorial/instance.c)

@[1](Create the instance.)
@[3-4](Create a material for this instance.)
@[6](Commit the material.)
@[8](Put the material on the instance.)
@[10](And finally commit the instance.)

---

### Create the world

@code[c++](code/ospTutorial/world.c)

@[1](Create the world.)
@[3-4](Create the array of instances in the world. (just one here))
@[6](Set the geometry instance array on the world.)
@[8](And finally commit the world.)

---

### Now we are ready to render a frame!

---

### Render a frame

@code[c++](code/ospTutorial/render_frame.c)

@[1](Kick off the render using the blocking version of ospRenderFrame())
@[3-4](Get the resulting pixels from the frame buffer.)
@[6](Do something with those pixels!)

---

For a more complete single-file tutorial, see _ospTutorial.c_ in the OSPRay repository.

---

# A Generalized Rendering Stack

---

### Today's Rendering Stack

![logo](assets/img/ospray_stack/slide10.png)

---

### Generic Rendering Stack

![logo](assets/img/OSPRay_stack/Slide01.png)

---

### Generic Rendering Stack

![logo](assets/img/OSPRay_stack/Slide02.png)

---

### Generic Rendering Stack

![logo](assets/img/OSPRay_stack/Slide03.png)

---

### Generic rendering stack

![logo](assets/img/ospray_stack/slide04.png)

---

### Generic rendering stack

![logo](assets/img/ospray_stack/slide05.png)

---

### Generic rendering stack

![logo](assets/img/ospray_stack/slide06.png)

---

### Generic rendering stack

![logo](assets/img/ospray_stack/slide07.png)

---

### Generic rendering stack

![logo](assets/img/ospray_stack/slide08.png)

---

### Generic rendering stack

![logo](assets/img/ospray_stack/slide09.png)

---

### Let's go from this...

---

### Generic rendering stack

![logo](assets/img/ospray_stack/slide10.png)

---

### To this!

---

### Generic rendering stack

![logo](assets/img/ospray_stack/slide11.png)

---

# _FIN_

### Questions?
